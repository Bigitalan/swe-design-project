package LotteryDrawing;

/*** Team4 Lottery Drawing
 *** Authors: Mustafa Rafati 1061585
 *** and Mehmet Baris, Arslan 1055973 ***/

public class RandNumGen extends Ticket {
	//attributes
	private  int[] randTicket=new int[6];
	private  int[] validNumbers=new int[6];
	private  int j=0,i=0,randNumber;
	//methods
	public void createRandTicket(){
		while(i<6){
			randNumber=(int) (Math.random()*48)+1;
			if(j==7){
				j=0;
			}
			for(j=0;j<6;j++){
					
				if(randNumber==validNumbers[j]){
					j=6;
				}

				else if(j==5){
					randTicket[i]=randNumber;
					validNumbers[i]=randNumber;
					i=i+1;						
				}
			}//for loop
		}//while loop
	}//method
	public RandNumGen getRanGenObject(){
		return RandNumGen.this;
	}
	public int[] getRanTicket(){
		return randTicket;
	}
}
