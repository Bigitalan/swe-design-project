package LotteryDrawing;

import java.util.Arrays;
import java.util.Scanner;

/*** Team4 Lottery Drawing
 *** Authors: Mustafa Rafati 1061585
 *** and Mehmet Baris, Arslan 1055973 ***/

public class Ticket {
	private Scanner scan = new Scanner(System.in);
	private int[] ticket = new int[6];
	private int[] validNumbers = {0,0,0,0,0,0}; 
	int i=0,j=0, userInput;
	
	public int[] userEnterNumber(){

		while(i<6){
			
			if(j==7){
				System.out.println("Number is already in use, try again!\n");
				j=0;
			}
			System.out.println("Please enter a number(between 1 and 49):\n"+
					Arrays.toString(validNumbers));
			
			//Check if input is a number
			while (!scan.hasNextInt()){  
				scan.next();
				System.out.println("only numbers!");
			}
			userInput = scan.nextInt();
			

			//First check the numbers range
			if(userInput>=1 && userInput<=49){
				//Then check if the number is already in use
				for(j=0;j<6;j++){
					//If a value is already used set j to 6 so the "else if(j==6)" is triggered
					if(userInput==validNumbers[j]){
						j=6;
					}
					//If everything is right set the values and increase the while loop
					else if(j==5){
						ticket[i]=userInput;
						validNumbers[i]=userInput;
						i=i+1;
						//j=0;
					}
				}
			}

			else{System.out.println("Number is out of range!");}
			
		}//while
		return ticket;
	}//ticket creating method
}
