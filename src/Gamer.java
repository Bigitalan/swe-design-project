package LotteryDrawing;

import java.util.Arrays;
import java.util.Scanner;

/*** Team4 Lottery Drawing
 *** Authors: Mustafa Rafati 1061585
 *** and Mehmet Baris, Arslan 1055973 ***/


public class Gamer {
	//attributes
	private String name;
	private int[] ticket = new int[6];
	private Scanner scan = new Scanner(System.in);
	private Ticket ticketObject;

	//constructors
	public Gamer(){
		System.out.println("Please enter your name:");
		setName();
		ticketObject = new Ticket();
		setTicket(ticketObject.userEnterNumber());
		System.out.println("Es wurde ein Lottoschein f�r: "+name+" erstellt!\n"+
		Arrays.toString(ticket));
	}
	
	//methods
	public String getName(){
		return name;
	}
	public void setName(){
		name=scan.next();
	}
	public int[] getTicket(){
		return ticket;
	}
	public void setTicket(int ticket[]){
		this.ticket=ticket;
	}
}
