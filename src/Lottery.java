package LotteryDrawing;
import java.util.ArrayList;
import java.util.Arrays;

/*** Team4 Lottery Drawing
 *** Authors: Mustafa Rafati 1061585
 *** and Mehmet Baris, Arslan 1055973 ***/

public class Lottery {
	//Attributes
	public ArrayList<Gamer> arrayUserList = new ArrayList<Gamer>();
	//Methods
	public void showAll(RandNumGen saveRNGObj){
		int hits = 0,userCount=1;
		String name;
		
		int[] arrayProxy1 = new int[6];
		int[] arrayProxy2 = new int[6];
		for(int i=0;i<arrayUserList.size();i++){
			hits=0;
			//return the user(name), the arrays of the tickets
			name=arrayUserList.get(i).getName();
			arrayProxy1=arrayUserList.get(i).getTicket();
			arrayProxy2=saveRNGObj.getRanTicket();
			//search for the right setted numbers
			for(int k=0;k<6;k++){
				if(arrayProxy1[k]==arrayProxy2[k]){
					hits++;
				}
			}
		System.out.println(userCount+". "+name+" with the lottery-ticket "+Arrays.toString(arrayProxy1)+" had "+hits+" hits!");	
		userCount++;
		}
		System.out.println(Arrays.toString(arrayProxy2));
	}
	

//***************************************************************************************************************************************//	


//***************************************************************************************************************************************//	

	public static void main(String[] args){
			InputOutput ioObject = new InputOutput();
			ioObject.selectOption();

	}
}
