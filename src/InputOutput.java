package LotteryDrawing;

import java.util.Arrays;
import java.util.Scanner;

/*** Team4 Lottery Drawing
 *** Authors: Mustafa Rafati 1061585
 *** and Mehmet Baris, Arslan 1055973 ***/

public class InputOutput {
	//Attributes
	private int input=-1,chronoTrigger=0;
	private Lottery lottObj = new Lottery();
	RandNumGen saveRNGObj;//this object is created in the class:RandNumGen but have to be saved here
	//Methods
	public void userInterface(){
		System.out.println("=== Lottery Menu ===\n1 Enter ticket");
		System.out.println("2 Lottery drawing\n3 Show result");
		System.out.println("0 Quit\nPlease enter (0-3):");
	}

	public void selectOption() {
		
		Scanner scan = new Scanner(System.in);
		while(input!=0){
			userInterface();
			
			while (!scan.hasNextInt()){  
				scan.next();
				System.out.println("only numbers!");
			}
			input=scan.nextInt();
			
			switch(input){
				case 1:
					if(chronoTrigger<=1){
						lottObj.arrayUserList.add(new Gamer());
						chronoTrigger=1;
					}
					else{System.out.println("This option isn`t available!");}
				break;
				
				case 2:
					if(chronoTrigger>=1 && lottObj.arrayUserList!=null){
						RandNumGen rngObject = new RandNumGen();
						System.out.println("A random ticket has been created.");
						rngObject.createRandTicket();
						saveRNGObj=rngObject.getRanGenObject();//saving the ticket object
						System.out.println(Arrays.toString(rngObject.getRanTicket()));
						chronoTrigger=chronoTrigger+1;
					}
					else{
						System.out.println("No access until option 1 has been used once!");
					}
				break;
				
				case 3:
					if(chronoTrigger>=2){
						System.out.println("Print all the users and their tickets!");
						lottObj.showAll(saveRNGObj);
					}

					else{
						System.out.println("No access until option 1 and 2 has been used once!");
					}
				break;
			}//switch
			
		}//while loop
		scan.close();
	}//method
	
}
